/*
    Five Game
    Copyright (C) 2012  Ray Fung

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdlib.h>
#include <time.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>

#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768
#define BPP 32
#define NONE 0
#define PLAYER 1
#define COMPUTER (-1)

SDL_Surface *black = NULL;
SDL_Surface *white = NULL;
SDL_Surface *screen = NULL;
SDL_Surface *board = NULL;
SDL_Surface *message = NULL;
SDL_Surface *selection = NULL;
SDL_Surface *cursor = NULL;

SDL_Event event;

TTF_Font *font = NULL;
SDL_Color text_color = {0x0, 0x0, 0xff}; /* 蓝色 */

int index_x = 7, index_y = 7; /* 当前光标位置 */
int chess_board[15][15] = {0};
int winner = NONE; /* 获胜者 */

/* 上一次下的棋子的位置，用于悔棋 */
int computer_last_move_row = -1, computer_last_move_column = -1;
int player_last_move_row = -1, player_last_move_column = -1;

/*
 * 功能：SDL 初始化
 * 返回值：成功返回1, 失败返回0
 */
int init()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) == -1)
		return 0;
	SDL_WM_SetCaption("五子棋", NULL);
	if((screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, BPP,
					SDL_SWSURFACE | SDL_FULLSCREEN)) == NULL)
		return 0;
	if(TTF_Init() == -1)
		return 0;
	return 1;
}

/*
 * 功能：清理占用的资源
 */
void clean_up()
{
	if(black)
		SDL_FreeSurface(black);
	if(white)
		SDL_FreeSurface(white);
	if(board)
		SDL_FreeSurface(board);
	if(message)
		SDL_FreeSurface(message);
	if(selection)
		SDL_FreeSurface(selection);
	if(font)
		TTF_CloseFont(font);
	TTF_Quit();
	SDL_Quit();
}

/*
 * 功能：延时 ms 毫秒，延时期间不处理任何事件
 */
void game_delay(int ms)
{
	int t;
	t = SDL_GetTicks();
	while(SDL_GetTicks() - t < ms)
		SDL_PollEvent(&event);
}

/*
 * 功能：等待某些按键才返回，除非已经超过了指定的时间
 * keys: 需要等待的按键(可以多个)
 * ms: 最大等待时间(单位是毫秒)，如果是-1,则不设置最大等待时间
 */
void wait_for_confirm(int ms)
{
	int t;
	t = SDL_GetTicks();
	while(ms == -1 || SDL_GetTicks() - t < ms)
	{
		if(SDL_PollEvent(&event))
		{
			if(event.type == SDL_KEYDOWN)
				switch(event.key.keysym.sym)
				{
					case SDLK_RETURN:
					case SDLK_SPACE:
						return;
				}
			else if(event.type == SDL_MOUSEBUTTONUP)
				return;
		}
	}
}

/*
 * 功能：将src绘制到以(x,y)为起始点的dst上面
 * src: 源图像指针
 * dst: 目标图像指针
 * x: 横坐标(水平向右)
 * y: 纵坐标(垂直向下)
 */
void apply_surface(SDL_Surface *src, SDL_Surface *dst, int x, int y)
{
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;
	SDL_BlitSurface(src, NULL, dst, &offset);
}

/*
 * 功能：将文件名为file_name的图像文件加载进来并转换为屏幕的显示格式，
 * 同时设置(0,255,255)颜色为透明色
 * file_name: 文件名，最好只出现ASCII字符
 * 返回值：如果转换成功，则返回转换后的 Surface 指针，否则返回 NULL
 */
SDL_Surface *load_image(const char file_name[])
{
	SDL_Surface *loaded_img = NULL;
	SDL_Surface *optimized_img = NULL;
	if((loaded_img = IMG_Load(file_name)))
	{
		optimized_img = SDL_DisplayFormat(loaded_img);
		if(optimized_img)
		{
			SDL_SetColorKey(optimized_img, SDL_SRCCOLORKEY,
							SDL_MapRGB(optimized_img->format, 0x0, 0xff, 0xff));
		}
	}
	return optimized_img;
}

/*
 * 功能：加载所需的各种图片以及字体
 * 返回值：所有资源都加载成功则返回非零值，否则返回零
 */
int prepare_data()
{
	font = TTF_OpenFont("wqy-microhei.ttc", 36);
	board = load_image("board.png");
	white = load_image("white.png");
	black = load_image("black.png");
	selection = load_image("selection.png");
	cursor = load_image("cursor.png");
	return (board && white && black && font && selection && cursor);
}

/*
 * 功能：填充屏幕背景颜色为黑色，并将棋盘绘制到屏幕上
 */
void draw_board()
{
	/* 填充黑色背景 */
	SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, 0x0, 0x0, 0x0));
	/* 居中显示棋盘 */
	apply_surface(board, screen, (SCREEN_WIDTH - board->w) / 2,
				  (SCREEN_HEIGHT - board->h) / 2);
}

/*
 * 功能：显示欢迎信息
 * 返回值：成功显示欢迎信息则返回1,否则返回0
 */
int welcome()
{
	/* FIX ME */
	/* 全屏模式下需要加上延时，否则看不到欢迎屏幕，原因未知 */
	game_delay(2000);

	draw_board();
	if((message = TTF_RenderUTF8_Blended(font, "欢迎来到五子棋的世界！", text_color)) == NULL)
		return 0;
	apply_surface(message, screen, (SCREEN_WIDTH - message->w) / 2, 50);
	if(SDL_Flip(screen) == -1)
		return 0;
	wait_for_confirm(3000);
	return 1;
}

/*
 * 功能：将一个图像绘制在屏幕的中央
 * item: 需要绘制的图像的指针
 * index_x: 水平序号(列号)
 * index_y: 垂直序号(行号)
 */
void draw_item(SDL_Surface *item, int index_x, int index_y)
{
	apply_surface(item, screen,
				  (SCREEN_WIDTH - board->w) / 2 + 20 + index_x * 35 - item->w / 2,
				  (SCREEN_HEIGHT - board->h) / 2 + 20 + index_y * 35 - item->h / 2);
}

/*
 * 功能：在屏幕的上方显示一串文字
 * str: 要显示的字符串(UTF-8 编码)
 */
void print_info(const char str[])
{
	if(message)
		SDL_FreeSurface(message);
	message = TTF_RenderUTF8_Blended(font, str, text_color);
	if(message)
	{
		apply_surface(message, screen, (SCREEN_WIDTH - message->w) / 2, 50);
		SDL_Flip(screen);
	}
}

/*
 * 功能：光标左移(如果不超出棋盘的话)
 */
void move_left()
{
	if(index_x > 0)
		--index_x;
}

/*
 * 功能：光标右移(如果不超出棋盘的话)
 */
void move_right()
{
	if(index_x < 14)
		++index_x;
}

/*
 * 功能：光标上移(如果不超出棋盘的话)
 */
void move_up()
{
	if(index_y > 0)
		--index_y;
}

/*
 * 功能：光标下移(如果不超出棋盘的话)
 */
void move_down()
{
	if(index_y < 14)
		++index_y;
}

/*
 * 功能：实现五子棋的AI功能(寻找最佳下棋位置)
 * row: 返回最佳位置的行号
 * column: 返回最佳位置的列号
 * me: 有 PLAYER 和 COMPUTER 两种值，
 * 表示当前的玩家（人或电脑）
 * 返回值：如果还有棋子可以下的话，返回最佳位置的权值，
 * 否则，返回-1
 */
int five_ai(int *row, int *column, int me)
{
	int r, c;
	int count[2], t, live;
	int user[2] = {PLAYER, COMPUTER};
	/* 主、辅对角线，同一行，同一列 */
	int step[4][2] = {{1, 1}, {-1, 1}, {0, 1}, {1, 0}};
	int i, j;
	int max = -1;
	int row_m, column_m;

	for(*row = 0; *row < 15; ++(*row))
		for(*column = 0; *column < 15; ++(*column))
		{
			if(chess_board[*row][*column] != NONE)
				continue;
			for(i = 0; i <= 1; ++i)
			{
				count[i] = 0;
				for(j = 0; j < 4; ++j)
				{
					t = 1; live = 0;
					r = *row + step[j][0];
					c = *column + step[j][1];
					while(r >= 0 && r < 15 && c >= 0 && c < 15)
					{
						if(chess_board[r][c] == -user[i])
							break;
						else if(chess_board[r][c] == user[i])
							++t;
						else 
						{
							++live;
							break;
						}
						r += step[j][0]; c += step[j][1];
					}

					r = *row - step[j][0];
					c = *column - step[j][1];
					while(r >= 0 && r < 15 && c >= 0 && c < 15)
					{
						if(chess_board[r][c] == -user[i])
							break;
						else if(chess_board[r][c] == user[i])
							++t;
						else 
						{
							++live;
							break;
						}
						r -= step[j][0]; c -= step[j][1];
					}
					if(t >= 5 && user[i] == me) t = 200;
					else if(t >= 5 && user[i] == -me) t = 100;
					else if(live == 0) t = 0;
					else if(t == 3 && live == 1) t = 5;
					else if(t == 3 && live == 2) t = 9;
					else if(t == 4 && live == 1) t = 10;
					else if(t == 4 && live == 2) t = 50;
					count[i] += t;
				}
			}
			if(count[0] + count[1] > max) 
			{
				/* 找到了更佳的位置 */
				max = count[0] + count[1];
				row_m = *row;
				column_m = *column;
			}
			else if(count[0] + count[1] == max && rand() % 2)
			{
				row_m = *row;
				column_m = *column;
			}
		}
	*row = row_m;
	*column = column_m;
	return max;
}

/*
 * 功能：画出所有的棋子(必须在画完棋盘之后再调用)
 */
void draw_all_chessman()
{
	int row, column;
	for(row = 0; row < 15; ++row)
		for(column = 0; column < 15; ++column)
		{
			/* 玩家是黑子，电脑是白子 */
			if(chess_board[row][column] == PLAYER)
				draw_item(black, column, row);
			else if(chess_board[row][column] == COMPUTER)
				draw_item(white, column, row);
		}
}

/*
 * 功能：玩家或者电脑下一个棋子
 * user: 玩家类型(PLAYER 或 COMPUTER)
 * 返回值：如果能够下棋，则返回1, 否则返回0
 */
int add_chess(int user)
{
	int i;
	/* 主、辅对角线，同一行，同一列 */
	int step[4][2] = {{1, 1}, {-1, 1}, {0, 1}, {1, 0}};
	int row, column;
	int count;
	int r, c;
	if(user == PLAYER)
	{
		if(chess_board[index_y][index_x] != NONE)
			return 0;
		row = index_y;
		column = index_x;
	}
	else if(five_ai(&row, &column, COMPUTER) == -1)
	{
		winner = NONE;
		return 0;
	}

	/* 记录悔棋操作所需的数据 */
	if(user == PLAYER)
	{
		player_last_move_row = row;
		player_last_move_column = column;
	}
	else
	{
		computer_last_move_row = row;
		computer_last_move_column = column;
	}

	chess_board[row][column] = user;
	draw_board();
	draw_all_chessman();
	draw_item(selection, index_x, index_y);

	/* 在四个方向上寻找五子连珠是否存在 */
	for(i = 0; i < 4; ++i)
	{
		count = 1;
		r = row + step[i][0];
		c = column + step[i][1];
		while(r >= 0 && r < 15 && c >= 0 && c < 15
				&& chess_board[r][c] == user)
		{
			++count;
			r += step[i][0];
			c += step[i][1];
		}

		r = row - step[i][0];
		c = column - step[i][1];
		while(r >= 0 && r < 15 && c >= 0 && c < 15
				&& chess_board[r][c] == user)
		{
			++count;
			r -= step[i][0];
			c -= step[i][1];
		}

		if(count >= 5)
		{
			winner = user;
			return 1;
		}
	}

	return 1;
}

/*
 * 功能：重新初始化数据，用于开始新游戏
 */
void reset_data()
{
	int row, column;
	winner = NONE;
	index_x = index_y = 7;
	player_last_move_row = player_last_move_column = -1;
	computer_last_move_row = computer_last_move_column = -1;
	for(row = 0; row < 15; ++row)
		for(column = 0; column < 15; ++column)
			chess_board[row][column] = NONE;
}

/*
 * 功能：悔棋
 */
void retract()
{
	if(player_last_move_row >= 0 && player_last_move_column >= 0
			&& computer_last_move_row >= 0 && computer_last_move_column >= 0)
	{
		chess_board[player_last_move_row][player_last_move_column] = NONE;
		chess_board[computer_last_move_row][computer_last_move_column] = NONE;
		player_last_move_row = player_last_move_column = -1;
		computer_last_move_row = computer_last_move_column = -1;
	}
}

/*
 * 功能：将鼠标光标的位置坐标转换为棋盘的行列序号
 * x: 鼠标光标横坐标
 * y: 鼠标光标纵坐标
 * row: 棋盘行号指针
 * column: 棋盘列号指针
 * 返回值：如果鼠标光标没有超出棋盘的范围则返回非零值，否则返回零
 */
int get_mouse_position_index(int x, int y, int *row, int *column)
{
	int left, top;
	left = (SCREEN_WIDTH - board->w) / 2 + 20 - 35 / 2;
	top = (SCREEN_HEIGHT - board->h) / 2 + 20 - 35 / 2;
	*column = (x - left) / 35;
	*row = (y - top) / 35;
	if(*row >= 0 && *row < 15
			&& *column >= 0 && *column < 15)
		return 1;
	else return 0;
}

/*
 * 功能：进行一个回合(玩家下一个棋子，电脑下一个棋子)
 * 并判断输赢和打平
 */
void one_round()
{
	int ok;

	/* 玩家在当前光标位置下一个棋子 */
	ok = add_chess(PLAYER);
	if(winner == PLAYER)
	{
		print_info("恭喜您，您赢了！");
		wait_for_confirm(-1);
		reset_data();
		return;
	}
	if(ok)
	{
		/* 电脑AI下棋 */
		ok = add_chess(COMPUTER);
		if(!ok)
		{
			print_info("居然打平了！");
			wait_for_confirm(-1);
			reset_data();
			return;
		}
		if(winner == COMPUTER)
		{
			print_info("很遗憾，您输了！");
			wait_for_confirm(-1);
			reset_data();
			return;
		}
	}
}

/*
 * 功能：游戏主循环，处理游戏逻辑等
 * 返回值：出错则返回非零值，否则，返回零
 */
int game_loop()
{
	int mouse_pos_x = SCREEN_WIDTH / 2, mouse_pos_y = SCREEN_HEIGHT / 2;
	SDL_WarpMouse(mouse_pos_x, mouse_pos_y);
	draw_board();
	draw_item(selection, index_x, index_y);
	if(SDL_Flip(screen) == -1)
		return 1;

	while(1)
	{
		Uint8 *key_state;
		int op = 0;
		int row, column;

		if(SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym)
					{
						case SDLK_ESCAPE:
							return 0;
						case SDLK_t:
							/* 提示最佳下棋位置 */
							five_ai(&index_y, &index_x, PLAYER);
							break;
						case SDLK_u:
							retract(); /* 悔棋 */
							break;
						case SDLK_f:
						case SDLK_RETURN:
						case SDLK_SPACE:
							one_round();
							mouse_pos_x = (SCREEN_WIDTH - board->w) / 2 + 20 + index_x * 35;
							mouse_pos_y = (SCREEN_HEIGHT - board->h) / 2 + 20 + index_y * 35;
							SDL_WarpMouse(mouse_pos_x, mouse_pos_y);
							break;
					}
					break;
				case SDL_MOUSEBUTTONUP:
					if(event.button.button == SDL_BUTTON_LEFT
							&& get_mouse_position_index(event.button.x, event.button.y,
								&row, &column))
					{
						one_round();
						mouse_pos_x = (SCREEN_WIDTH - board->w) / 2 + 20 + index_x * 35;
						mouse_pos_y = (SCREEN_HEIGHT - board->h) / 2 + 20 + index_y * 35;
						SDL_WarpMouse(mouse_pos_x, mouse_pos_y);
					}
					else if(event.button.button == SDL_BUTTON_RIGHT)
						retract(); /* 悔棋 */
					else if(event.button.button == SDL_BUTTON_MIDDLE)
					{
						/* 提示最佳下棋位置 */
						five_ai(&index_y, &index_x, PLAYER);
						mouse_pos_x = (SCREEN_WIDTH - board->w) / 2 + 20 + index_x * 35;
						mouse_pos_y = (SCREEN_HEIGHT - board->h) / 2 + 20 + index_y * 35;
						SDL_WarpMouse(mouse_pos_x, mouse_pos_y);
					}
					break;
				case SDL_MOUSEMOTION:
					mouse_pos_x = event.motion.x;
					mouse_pos_y = event.motion.y;
					if(get_mouse_position_index(event.motion.x, event.motion.y,
								&row, &column))
					{
						index_x = column;
						index_y = row;
					}
					break;
				case SDL_QUIT:
					return 0;
			}
		}

		/* 支持三种移动光标的方式：方向键、VIM、WASD */
		/* 而且，支持斜线移动 */
		key_state = SDL_GetKeyState(NULL);
		if(key_state[SDLK_UP] || key_state[SDLK_k] || key_state[SDLK_w])
			op |= 0x1;
		if(key_state[SDLK_DOWN] || key_state[SDLK_j] || key_state[SDLK_s])
			op |= 0x2;
		if(key_state[SDLK_LEFT] || key_state[SDLK_h] || key_state[SDLK_a])
			op |= 0x4;
		if(key_state[SDLK_RIGHT] || key_state[SDLK_l] || key_state[SDLK_d])
			op |= 0x8;
		switch(op)
		{
			case 1:
				move_up();
				break;
			case 2:
				move_down();
				break;
			case 4:
				move_left();
				break;
			case 5:
				move_up(); move_left();
				break;
			case 6:
				move_down(); move_left();
				break;
			case 8:
				move_right();
				break;
			case 9:
				move_up(); move_right();
				break;
			case 10:
				move_down(); move_right();
				break;
		}

		draw_board();
		draw_all_chessman();
		draw_item(selection, index_x, index_y);
		apply_surface(cursor, screen, mouse_pos_x, mouse_pos_y);
		SDL_Flip(screen);
		if(op)
			SDL_Delay(SDL_DEFAULT_REPEAT_INTERVAL * 4);
	}
}

int main(int argc, char *argv[])
{
	srand(time(0)); /* 初始化随机数生成器,后面的AI算法要用到随机数 */
	if(init() == 0)
	{
		clean_up();
		return 1;
	}
	
	if(prepare_data() == 0)
	{
		clean_up();
		return 1;
	}

	/* 隐藏鼠标光标，因为我们要使用自定义的鼠标光标 */
	SDL_ShowCursor(SDL_DISABLE);

	/* 显示欢迎屏幕 */
	if(welcome() == 0)
	{
		clean_up();
		return 1;
	}

	/* 游戏主循环 */
	game_loop();

	clean_up();
	return 0;
}

